let wygiEditor = function(editor) {
    let html = document.querySelector(editor);

    let toolbars = {
        init: function(){
            this.icons.addIconClass();
            this.toolBarButtons.addButtonId();
            this.addClass();
            this.addIcons();
            this.appendButtons();
            this.editorContent();
            this.font_size();
            this.defaultBehavior();
            this.colors();
            this.fontNames();
            // this.table();
            // this.createModalContent();
        },

        defaultBehavior: function(){
            document.execCommand('defaultParagraphSeparator', false , 'p');
            document.execCommand('insertBrOnReturn', false , 'br');
        },

        //toolbar wrapper add class
        addClass: function(){
            this.toolBarWrapper.className = 'wrapper';
            this.editor.className = 'editableContainer';
            this.toolBarButtons.source.className = 'source';
            this.toolBarButtons.fontsize.className = 'font-size';
            this.toolBarButtons.color_picker.className = 'color';
            this.toolBarButtons.fontname.className = 'fontname';
            this.toolBarButtons.table.className = 'table';
        },

        //add icons
        addIcons: function(){
            this.toolBarButtons.bold.appendChild(this.icons.bold);
            this.toolBarButtons.align_center.appendChild(this.icons.aligncenter);
            this.toolBarButtons.align_left.appendChild(this.icons.alignleft);
            this.toolBarButtons.align_right.appendChild(this.icons.alignright);
            this.toolBarButtons.align_full.appendChild(this.icons.alignfull);
            this.toolBarButtons.fontsize.appendChild(this.icons.fontsize);
            this.toolBarButtons.source.appendChild(this.icons.source);
            this.toolBarButtons.erase.appendChild(this.icons.erase);
            this.toolBarButtons.color_picker.appendChild(this.icons.color);
            this.toolBarButtons.orderedlist.appendChild(this.icons.orderedlist);
            this.toolBarButtons.fontname.appendChild(this.icons.fontname);
            this.toolBarButtons.table.appendChild(this.icons.table);
        },

        //add the buttons to toolbar
        appendButtons: function(){
            this.toolBarWrapper.appendChild(this.toolBarButtons.bold);
            this.toolBarWrapper.appendChild(this.toolBarButtons.fontname);
            this.toolBarWrapper.appendChild(this.toolBarButtons.align_center);
            this.toolBarWrapper.appendChild(this.toolBarButtons.align_left);
            this.toolBarWrapper.appendChild(this.toolBarButtons.align_right);
            this.toolBarWrapper.appendChild(this.toolBarButtons.align_full);
            this.toolBarWrapper.appendChild(this.toolBarButtons.orderedlist);
            this.toolBarWrapper.appendChild(this.toolBarButtons.fontsize);
            this.toolBarWrapper.appendChild(this.toolBarButtons.erase);
            this.toolBarWrapper.appendChild(this.toolBarButtons.color_picker);
            this.toolBarWrapper.appendChild(this.toolBarButtons.table);
            this.toolBarWrapper.appendChild(this.toolBarButtons.source);

            //maintent which is the editable section
            this.toolBarWrapper.appendChild(this.editor);
        },

        editorContent: function() {
            this.editor.setAttribute('contenteditable', true);
            this.editor.setAttribute('spellcheck', false);
        },

        font_size: function(){
            let size = ['1','2','3','4','5','6','7'];
            let ul = document.createElement('ul');
            ul.className = 'sizes';
            
            for(sizes in size){
                let li = document.createElement('li');
                let textNode = document.createTextNode(size[sizes]);
                li.appendChild(textNode);

                ul.appendChild(li);
            }

            this.toolBarButtons.fontsize.appendChild(ul);
        },

        colors: function() {
            let colors = ['#2ecc71','#3498db','#95a5a6','#9b59b6','#2c3e50','#f39c12','#e67e22','#ecf0f1'];
            let ul = document.createElement('ul');
            ul.className = 'colors';

            for(color in colors) {
                let li = document.createElement('li');
                let textNode = document.createTextNode(colors[color]);
                li.appendChild(textNode);

                ul.appendChild(li);
            }

            this.toolBarButtons.color_picker.appendChild(ul);
        },

        fontNames: function() {
            let names = ['Arial','Calibri','system-ui','tahoma'];
            let ul = document.createElement('ul');
            ul.className = 'fontnames';

            for(name in names) {
                let li = document.createElement('li');
                let textNode = document.createTextNode(names[name]);
                li.appendChild(textNode);

                ul.appendChild(li);
            }

            this.toolBarButtons.fontname.appendChild(ul);
        },

        // table: function() {
        //     let table = document.createElement('div');
        //     let rows = 10;
        //     let columns = 18;
        //     let count = 1;
        //     table.className = 'customTable';

        //     for(count; count <= rows; count++) {
        //         let row = document.createElement('div');
        //         let countColumns = 1;
        //         row.className = `row-${count}`;

        //         for(countColumns; countColumns <= columns; countColumns++) {
        //             let column = document.createElement('p');
        //             column.className = `column-${countColumns}`;

        //             row.appendChild(column);
        //         }

        //         table.appendChild(row);
        //     }

        //     this.toolBarButtons.table.appendChild(table);
        // },
        
        //specify icons
        icons: {
            addIconClass: function(){
                this.bold.className = 'fa fa-bold';
                this.aligncenter.className =  'fa fa-align-center';
                this.alignleft.className = 'fa fa-align-left';
                this.alignright.className = 'fa fa-align-right';
                this.alignfull.className = 'fa fa-align-justify';
                this.source.className = 'fa fa-code';
                this.fontsize.className = 'fa fa-text-height';
                this.erase.className = 'fa fa-eraser';
                this.color.className = 'fa fa-tint';
                this.orderedlist.className = 'fa fa-list-ol';
                this.fontname.className = 'fa fa-font';
                this.table.className = 'fa fa-table';

            },
            bold: document.createElement('i'),
            aligncenter: document.createElement('i'),
            alignleft: document.createElement('i'),
            alignright: document.createElement('i'),
            alignfull: document.createElement('i'),
            source: document.createElement('i'),
            fontsize: document.createElement('i'),
            erase: document.createElement('i'),
            color: document.createElement('i'),
            orderedlist: document.createElement('i'),
            fontname: document.createElement('i'),
            table: document.createElement('i')
        },

        toolBarWrapper: document.createElement('div'),
        // modal: document.createElement('div'),
        editor: document.createElement('div'),

        toolBarButtons: {
            addButtonId: function(){
                this.bold.id = 'bold';
                this.align_center.id = 'justifyCenter';
                this.align_left.id = 'justifyLeft';
                this.align_right.id = 'justifyRight';
                this.align_full.id = 'justifyFull';
                this.erase.id = 'removeFormat';
                this.orderedlist.id = 'insertOrderedList';
            },
            bold: document.createElement('button'),
            align_center: document.createElement('button'),
            align_left: document.createElement('button'),
            align_right: document.createElement('button'),
            align_full: document.createElement('button'),
            source: document.createElement('button'),
            fontsize: document.createElement('button'),
            erase: document.createElement('button'),
            color_picker: document.createElement('button'),
            orderedlist: document.createElement('button'),
            fontname: document.createElement('button'),
            table: document.createElement('button')
        }

        // createModalContent: function() {
        //     let modalContent = document.createElement('div');
        //     let selectRows = document.createElement('select');
        //     let rows = 20;
        //     let columns = 18;

        //     modalContent.className = 'modal-content';
        //     selectRows.className = 'rows';

        //     this.modal.appendChild(modalContent);

        //     for(let i = 0; i < rows; i++) {
        //         let option = document.createElement('option');
        //         let textNode = document.createTextNode();
        //     }
        // }
    }

    toolbars.init();

    html.appendChild(toolbars.toolBarWrapper);
    // html.appendChild(toolbars.modal);

    let callBack = function(cmd, bool, val){
        document.execCommand(cmd, bool, val);
    }

    document.addEventListener('click', function(event){
        
        let fontsize = document.getElementsByClassName('sizes');
        let colors = document.getElementsByClassName('colors');
        let fontnames = document.getElementsByClassName('fontnames');
        let editor = document.querySelector('.editableContainer');

        (event.target.className !== 'font-size') ? fontsize[0].classList.remove('display') : '';
        (event.target.className !== 'color') ? colors[0].classList.remove('display') : '';
        (event.target.className !== 'fontname') ? fontnames[0].classList.remove('display') : '';

        (event.target.parentNode.className == 'sizes') ? callBack('fontSize', false, event.target.innerText) : '';
        (event.target.parentNode.className == 'colors') ? callBack('foreColor' , false , event.target.innerText) : '' ;
        (event.target.parentNode.className == 'fontnames') ? callBack('fontName', false , event.target.innerText) : '';
        (event.target.id == 'removeFormat') ? callBack(event.target.id, false, null) : '';
        (event.target.className == 'source') ?  console.log(editor.outerHTML) : '';
        (event.target.className == 'font-size') ? fontsize[0].classList.toggle('display') : '';
        (event.target.className == 'color') ? colors[0].classList.toggle('display') : '';
        (event.target.className == 'fontname') ? fontnames[0].classList.toggle('display') : '';
        (event.target.id !== '') ? callBack(event.target.id, false , null) : '';

    });

}